#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <sys/prctl.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>

#define LISTEN_BACKLOG 50
#define LISTEN_PORT 5000
#define STDIN 0

void print_ip(struct addrinfo * res) {
char str[128];

switch (res->ai_addr->sa_family)
{
  case AF_INET:
  {
      struct sockaddr_in * p = (struct sockaddr_in *)(res->ai_addr);
      if (inet_ntop(p->sin_family, &p->sin_addr, str, sizeof str) == NULL)
          fprintf(stderr, "Error in inet_ntop: %s\n", strerror(errno));
      else
          printf("Address: %08X (%s)\n", p->sin_addr.s_addr, str);
      break;
  }
  case AF_INET6:
  {
      struct sockaddr_in6 * p = (struct sockaddr_in6 *)(res->ai_addr);
      if (inet_ntop(p->sin6_family, &p->sin6_addr, str, sizeof str) == NULL)
          fprintf(stderr, "Error in inet_ntop: %s\n", strerror(errno));
      else
          printf("Address: %s\n", str);
      break;
  }
  default:
  {
      fprintf(stderr, "Unrecogized address family, skipping.\n");
  }
}
}

int main(int agrc, char** argv) {
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr;
    struct addrinfo hints, *res;
    int reuseaddr = 1; // true

    // Create a socket
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    if (listenfd == -1)
    {
        //RP_LOG(LOG_ERR, "Failed to create a socket (%s)", strerror(errno));
        perror("Failed to create a socket");
        return (EXIT_FAILURE);
    }
    printf("listenfd = %d\n", listenfd);

    // Enable the socket to reuse the address 
    //if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(int)) < 0) {
    if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0) {
        perror("setsockopt");
        return (EXIT_FAILURE);
    }

    int flags;
    flags = fcntl(listenfd,F_GETFL,0);
    if (flags < 0)
    {
        //RP_LOG(LOG_ERR, "Failed to fcntl a socket (%s)", strerror(errno));
        perror("Failed to fcntl a socket");
        return (EXIT_FAILURE);
    }
    fcntl(listenfd, F_SETFL, O_NONBLOCK);

    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(LISTEN_PORT);

    memset(&hints, 0, sizeof hints);
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    if (getaddrinfo(NULL, "5000", &hints, &res) != 0) {
        perror("getaddrinfo");
        return (EXIT_FAILURE);;
    }
    
    while (res != NULL) {
    print_ip(res);
    char str[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(res->ai_addr->sa_data), str, INET_ADDRSTRLEN);
    printf("Server address "
           "(host=%s, port=%s)\n", str, res->ai_canonname);
    res = res->ai_next;
    }

    if (bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) == -1)
    {
        //RP_LOG(LOG_ERR, "Failed to bind the socket (%s)", strerror(errno));
        perror("Failed to bind the socket");
        return (EXIT_FAILURE);
    }
    //freeaddrinfo(serv_addr);

    if (listen(listenfd, LISTEN_BACKLOG) == -1)
    {
        //RP_LOG(LOG_ERR, "Failed to listen on the socket (%s)", strerror(errno));
        perror("Failed to listen on the socket");
        return (EXIT_FAILURE);
    }

    //RP_LOG(LOG_INFO, "Server is listening on port %d\n", LISTEN_PORT);
    printf("Server is listening on port %d\n", LISTEN_PORT);

    int clients[10];
    char client_buf[10][255];
    for (int i = 0; i < 10; i++) {
        clients[i] = -1;
        memset(&client_buf[i], 0, 255);
    }
    fd_set read_flags,write_flags; // the flag sets to be used
    struct timeval waitd;          // the max wait time for an event
    int sel;                      // holds return value for select();
    char in[255];
    char out[255];
    memset(&in, 0, 255);
    memset(&out, 0, 255);
    int numSent;
    int numRead;

    FD_ZERO(&read_flags);
    FD_ZERO(&write_flags);

    while(1) {
        int maxfd = 0;
        FD_SET(listenfd, &read_flags);
        FD_SET(listenfd, &write_flags);
        if (maxfd < listenfd) maxfd = listenfd;
	for (int i = 0; i < 10; i++) {
            if (clients[i] > 0) {
                FD_SET(clients[i], &read_flags);
                //FD_SET(clients[i], &write_flags);
                if (maxfd < clients[i]) maxfd = clients[i];
            }
        }
        FD_SET(STDIN_FILENO, &read_flags);
        //FD_SET(STDIN_FILENO, &write_flags);
        if (maxfd < STDIN_FILENO) maxfd = STDIN_FILENO;
        waitd.tv_sec = 5;
        waitd.tv_usec = 0;

        sel = select(10, &read_flags, &write_flags, (fd_set*)0, &waitd);
        if (sel < 0) {
            //RP_LOG(LOG_ERR, "Failed to accept connection (%s)", strerror(errno));
            perror("Failed to select connection\n");
            return (EXIT_FAILURE);
        }
        printf("sel = %d\n", sel);
        //if an error with select
        if(sel = 0)
            continue;

        //socket ready for reading
        if(FD_ISSET(listenfd, &read_flags)) {
            printf("listenfd = %d read_flags\n", listenfd);
            // there is new connect request, accept it
            struct sockaddr_storage cliaddr;
            socklen_t clilen;
            clilen = sizeof(cliaddr);

            connfd = accept(listenfd, (struct sockaddr *)&cliaddr, &clilen);
            if (connfd < 0) {
                if ((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
                    // We have processed all incoming connections.
                    break;
                } else {
                    //RP_LOG(LOG_ERR, "Failed to accept connection (%s)", strerror(errno));
                    perror("Failed to accept connection\n");
                    return (EXIT_FAILURE);
                }
            }
            printf("connfd = %d\n", connfd);
            char hbuf[255], sbuf[255];
            int s = getnameinfo ((struct sockaddr *)&cliaddr, clilen, hbuf, sizeof hbuf,
                 sbuf, sizeof sbuf, NI_NUMERICHOST | NI_NUMERICSERV);
            if (s == 0){
                printf("Accepted connection on descriptor %d "
                         "(host=%s, port=%s)\n", connfd, hbuf, sbuf);
            }

            //set non blocking
            flags = fcntl(connfd,F_GETFL,0);
            if (flags < 0)
            {
                //RP_LOG(LOG_ERR, "Failed to fcntl a socket (%s)", strerror(errno));
                perror("Failed to fcntl a socket");
                return (EXIT_FAILURE);
            }
            fcntl(connfd, F_SETFL, O_NONBLOCK);
            printf("\nNonblocking connfd!\n");

            // insert client in clients list
            for (int i = 0; i < 10; i++) {
                if (clients[i] < 0) {
                    clients[i] = connfd;
                    printf("client inserted clients[%d] = %d\n", i, clients[i]);
                    break;
                } else {
                    printf("client empty clients[%d] = %d\n", i, clients[i]);
                }
            }

            //clear set
            FD_CLR(listenfd, &read_flags);

        }   //end if ready for read

        //socket ready for writing
        if(FD_ISSET(listenfd, &write_flags)) {
            printf("listenfd = %d write_flags\n", listenfd);
            //printf("\nSocket ready for write");
            FD_CLR(listenfd, &write_flags);
            for (int i = 0; i < 10; i++) {
                if (clients[i] > 0 
                    && FD_ISSET(clients[i], &write_flags)) {
                    sprintf(out, "client %i:", i);
                    send(clients[i], out, 255, 0);
                    memset(&out, 0, 255);
                }
            }
        }   //end if

        for (int i = 0; i < 10; i++) {
            if (clients[i] > 0) {
                //printf("client test clients[%d] = %d\n", i, clients[i]);
                // test if client read enabled
                if (FD_ISSET(clients[i], &read_flags)) {
                    printf("client read_flags clients[%d] = %d\n", i, clients[i]);
                    // there are data from client, read it.
                    memset(&in, 0, 255);

                    numRead = recv(clients[i], in, 255, 0);
                    if(numRead <= 0) {
                        printf("\nClosing socket");
                        close(clients[i]);
                        FD_CLR(clients[i], &read_flags);
                        clients[i] = -1;
                        continue;
                    }
                    else if(in[0] != '\0')
                        printf("\nClients[%d] = %d receive: %s", i, clients[i], in);
                } else {
                    //printf("client is not read_flags clients[%d] = %d\n", i, clients[i]);
                }
                // test if client write enabled
                if (FD_ISSET(clients[i], &write_flags)) {
                    printf("client write_flags clients[%d] = %d\n", i, clients[i]);
                    // write data to client
                    //sprintf(out, "client %i:", i);
                    send(clients[i], client_buf[i], 255, 0);
                    memset(client_buf[i], 0, 255);
                    FD_CLR(clients[i], &write_flags);
                } else {
                    //printf("client is not write_flags clients[%d] = %d\n", i, clients[i]);
                }
            }
        }

        //if stdin is ready to be read
        if(FD_ISSET(STDIN_FILENO, &read_flags)) {
            printf("stdin read_flags\n");
            fgets(out, 255, stdin);
	    for (int i = 0; i < 10; i++) {
                if (clients[i] > 0) {
                    char *p = strchr(client_buf[i], 0);
                    strcpy(p, out);
                    FD_SET(clients[i], &write_flags);
                }
            }
            printf("cmp = %d\n", strcmp(out, "exit\n"));
            if (strcmp(out, "exit\n") == 0)
                break;
        }

    }   //end while
    close(listenfd);

    printf("\n\nExiting normally\n");
    return 0;
}
